import React from 'react'
import './../sass/main.scss'
import SEO from '../components/seo'
import MobileHeader from '../components/mobileHeader'
import MobileFooter from '../components/mobileFooter'
import Desktop from '../components/desktop'

const IndexPage = () => (
  <div className="home">
    <SEO
      title="Portal"
      keywords={[
        'ParticleMC',
        'ParticleMC Networks',
        'Minecraft',
        'Towny',
        'Factions',
        'Prison',
        'Creative',
        'SkyBlock',
        'Minecraft Server',
        'Network Server',
      ]}
    />
    <div className="mobile-site">
      <MobileHeader />
      <MobileFooter />
    </div>
    <div className="desktop-site">
      <Desktop />
    </div>
  </div>
)

export default IndexPage
