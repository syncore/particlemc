import React from 'react';
import home from './../images/home.svg';
import forums from './../images/forums.svg';
import store from './../images/store.svg';
import vote from './../images/vote.svg';
import banlist from './../images/banlist.svg';

const mobileFooter = () => {
	return (
		<div className="mobileFooter">
			<h2 className="heading__2">Our Links</h2>
			<div className="row center-xs middle-xs">
				<div className="col-xs-12 col-sm-6">
					<a href="https://home.particlemc.com" className="portal-link brown">
						<img src={home} alt="Home URL with house icon" draggable={false} className="portal-icon" />
						<p className="margin-negative-top">Home</p>
					</a>
				</div>
				<div className="col-xs-12 col-sm-6">
					<a href="https://forums.particlemc.com" className="portal-link orange">
						<img
							src={forums}
							alt="Forums URL with a page filled out with ink icon"
							draggable={false}
							className="portal-icon"
						/>
						<p className="margin-negative-top">Forums</p>
					</a>
				</div>
				<div className="col-xs-12 col-sm-6">
					<a href="https://shop.particlemc.com" className="portal-link yellow">
						<img
							src={store}
							alt="Store URL with a christmas gift icon"
							draggable={false}
							className="portal-icon"
						/>
						<p className="margin-negative-top">Store</p>{' '}
					</a>
				</div>
				<div className="col-xs-12 col-sm-6">
					<a href="https://vote.particlemc.com" className="portal-link blue">
						<img
							src={vote}
							alt="Vote URL with a uptrending stock market graph icon"
							draggable={false}
							className="portal-icon"
						/>
						<p className="margin-negative-top">Vote</p>
					</a>
				</div>
				<div className="col-xs-12 col-sm-6">
					<a href="https://bans.particlemc.com" className="portal-link red">
						<img
							src={banlist}
							alt="Ban List URL with a skull and bones icon"
							draggable={false}
							className="portal-icon"
						/>
						<p className="margin-negative-top">Ban List</p>
					</a>
				</div>
			</div>
			<footer>
				<p className="copyright" style={{ textAlign: 'center', width: '100%' }}>
					&copy; ParticleMC 2019. Website designed by{' '}
					<a href="https://shaunchander.me" className="athys" target="_blank" rel="noopener">
						Athys
					</a>
					. Special thanks to{' '}
					<a href="https://flaticon.com" className="athys" draggable="false" rel="noopener">
						Flaticon
					</a>{' '}
					for use of their icons.
				</p>
			</footer>
		</div>
	);
};

export default mobileFooter;
