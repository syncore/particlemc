import React from 'react';
import logo from './../images/Logo.png';
import Particles from 'react-particles-js';
import particles from '../particles';
import axios from 'axios';
import jump from 'jump.js';

class mobileHeader extends React.Component {
	constructor() {
		super();

		this.state = {
			isClicked: false,
			playerCount: 0
		};
	}

	componentDidMount() {
		axios
			.get('https://mcapi.us/server/status?ip=eu.particlemc.com')
			.then((res) => this.setState({ playerCount: res.data.players.now }))
			.catch((err) => console.log(err));
	}

	render() {
		return (
			<div className="mobileHeader">
				<Particles params={particles} className="particles" />
				<img
					src={logo}
					alt="ParticleMC's Logo, come play ParticleMC today at play.particlemc.net!"
					className="mobileHeader__logo animated pulse infinite"
				/>
				<h1 className="heading__1">
					Play with <span className="bold yellow">{this.state.playerCount}</span> other players today at{' '}
					<span className="yellow bold">play.particlemc.com</span>!
				</h1>
				<a
					href="#"
					className="link"
					onClick={(e) => {
						e.preventDefault();
						jump('.mobileFooter');
					}}
				>
					scroll to links below
				</a>
			</div>
		);
	}
}
export default mobileHeader;
