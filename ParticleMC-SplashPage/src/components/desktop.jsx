import React from 'react';
import './../sass/main.scss';
import axios from 'axios';

import logo from './../images/Logo.png';
import cursor from './../images/cursor.svg';

import home from '../images/home.svg';
import forums from '../images/forums.svg';
import store from '../images/store.svg';
import vote from '../images/vote.svg';
import banlist from '../images/banlist.svg';
import Particles from 'react-particles-js';
import particles from './../particles.json';
import copy from 'copy-to-clipboard';

class desktop extends React.Component {
	constructor() {
		super();

		this.state = {
			isClicked: false,
			playerCount: 0
		};
	}

	componentDidMount() {
		axios
			.get('https://mcapi.us/server/status?ip=eu.particlemc.com')
			.then((res) => this.setState({ playerCount: res.data.players.now }))
			.catch((err) => console.log(err));
	}
	render() {
		return (
			<div className="desktop">
				<Particles className="particles" params={particles} />
				<div style={{ zIndex: 10 }}>
					<div className="row middle-xs center-xs">
						<div className="col-md-4">
							<h1 className="heading__1 left">
								Play with <span className="bold yellow">{this.state.playerCount}</span> other<br />{' '}
								players today!
							</h1>
						</div>
						<div className="col-md-4">
							<img
								src={logo}
								alt="ParticleMC's logo! Play ParticleMC today at play.particlemc.com!"
								draggable={false}
								className={'desktop-logo animated pulse infinite'}
							/>
						</div>
						<div className="col-md-4">
							<button
								className="btn"
								aria-label="play.particlem.mc.net"
								onClick={() => {
									copy('play.particlemc.com');
									document.querySelector('.btn-text').textContent = 'Ip Copied!';

									setTimeout(() => {
										document.querySelector('.btn-text').textContent = 'play.particlemc.com';
									}, 2000);
								}}
							>
								<p className="btn-text">play.particlemc.com</p>
								<svg
									className="btn-cursor"
									width="20"
									height="20"
									viewBox="0 0 20 20"
									fill="none"
									xmlns="http://www.w3.org/2000/svg"
								>
									<g clip-path="url(#clip0)">
										<path
											className="btn-cursor-fill"
											d="M18.6943 0.0643615L0.629031 7.77761C0.29556 7.9226 0.0780714 8.25607 0.10708 8.61855C0.136088 8.98103 0.368056 9.29997 0.716007 9.40148L8.19728 11.6922L10.488 19.3765C10.5896 19.7245 10.9085 19.971 11.271 20C11.2855 20 11.3 20 11.3145 20C11.6624 20 11.9814 19.797 12.1119 19.4636L19.8252 1.19524C19.9702 0.861769 19.8832 0.484809 19.6367 0.238312C19.4047 0.00639449 19.0278 -0.0661016 18.6943 0.0643615ZM11.474 16.5638L9.74866 10.7498C9.66168 10.4744 9.44419 10.2569 9.16869 10.1699L3.51424 8.44455L17.4039 2.51465L11.474 16.5638Z"
											fill="#FAFAFA"
										/>
									</g>
									<defs>
										<clipPath id="clip0">
											<rect
												width="20"
												height="20"
												fill="white"
												transform="matrix(1 0 0 -1 0 20)"
											/>
										</clipPath>
									</defs>
								</svg>
							</button>
						</div>
					</div>
					<div className="row center-xs middle-xs">
						<div className="col-md-2">
							<a href="https://home.particlemc.com
" className="portal-link brown">
								<img
									src={home}
									alt="Home URL with house icon"
									draggable={false}
									className="portal-icon"
								/>
								<p className="margin-negative-top">Home</p>
							</a>
						</div>
						<div className="col-md-2">
							<a href="https://forums.particlemc.com" className="portal-link orange">
								<img
									src={forums}
									alt="Forums URL with a page filled out with ink icon"
									draggable={false}
									className="portal-icon"
								/>
								<p className="margin-negative-top">Forums</p>
							</a>
						</div>
						<div className="col-md-2">
							<a href="https://shop.particlemc.com" className="portal-link yellow">
								<img
									src={store}
									alt="Store URL with a christmas gift icon"
									draggable={false}
									className="portal-icon"
								/>
								<p className="margin-negative-top">Store</p>{' '}
							</a>
						</div>
						<div className="col-md-2">
							<a href="https://vote.particlemc.com
" className="portal-link blue">
								<img
									src={vote}
									alt="Vote URL with a uptrending stock market graph icon"
									draggable={false}
									className="portal-icon"
								/>
								<p className="margin-negative-top">Vote</p>
							</a>
						</div>
						<div className="col-md-2">
							<a href="https://bans.particlemc.com
" className="portal-link red">
								<img
									src={banlist}
									alt="Ban List URL with a skull and bones icon"
									draggable={false}
									className="portal-icon"
								/>
								<p className="margin-negative-top">Ban List</p>
							</a>
						</div>
					</div>
					<footer className="footer-desktop">
						<p className="copyright">
							&copy; ParticleMC 2019. Website designed by{' '}
							<a href="https://shaunchander.me" className="athys" target="_blank" rel="noopener">
								Athys
							</a>
							. Special thanks to{' '}
							<a href="https://flaticon.com" className="athys" draggable="false" rel="noopener">
								Flaticon
							</a>{' '}
							for use of their icons.
						</p>
					</footer>
				</div>
			</div>
		);
	}
}

export default desktop;
