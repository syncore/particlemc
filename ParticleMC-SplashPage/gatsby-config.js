module.exports = {
	siteMetadata: {
		title: `ParticleMC Networks`,
		description: `ParticleMC is a newly released network offering five widely demanded gamemodes. The gamemodes we offer ensure that all players passing through the network have a gamemode suited to them. Wether that be a competitive survival mode such as Factions or a more relaxed survival mode such as Towny. We also hold regular events on each of our servers to maintain player enjoyment.`,
		author: `Syncore`
	},
	plugins: [
		`gatsby-plugin-react-helmet`,
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `images`,
				path: `${__dirname}/src/images`
			}
		},
		`gatsby-transformer-sharp`,
		`gatsby-plugin-sharp`,
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `gatsby-starter-default`,
				short_name: `starter`,
				start_url: `/`,
				background_color: `#222`,
				theme_color: `#FFFF55`,
				display: `minimal-ui`,
				icon: `src/images/Logo.png` // This path is relative to the root of the site.
			}
		},
		// this (optional) plugin enables Progressive Web App + Offline functionality
		// To learn more, visit: https://gatsby.app/offline
		'gatsby-plugin-offline',
		`gatsby-plugin-sass`,
		{
			resolve: `gatsby-plugin-google-fonts`,
			options: {
				fonts: [ `Tajawal\:300,400,500,700,900` ] // you can also specify font weights and styles
			}
		}
	]
};
